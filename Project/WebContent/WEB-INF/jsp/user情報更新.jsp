<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <%-- EL式–カスタムタグ[4-28] --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新画面</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

<header>

	<ul>
		<li>ユーザ名  ${userInfo.name} さん</li>
		<li class="dropdown"> <a href="LogoutServlet" class="btn1">ログアウト</a></li>
	</ul>

</header>

<h1 align="center">ユーザ情報更新</h1>

<c:if test="${errMsg != null}" >
	<div class="alert alert-danger" role="alert">
		${errMsg}
	</div>
</c:if>

<form action="UserUpdateServlet" method="post">

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2"><b>ログインID</b></div> <div class="col-md-4">${user.loginId}</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2"><b>パスワード</b></div> <input type="password" class="col-md-4" name="password">
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>パスワード(確認)</b></div> <input type="password" class="col-md-4" name="confirmPassword">
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>ユーザ名</b></div> <% String name = (String) request.getAttribute("user.name"); %>
		<input type="text" class="col-md-4"  name="name" value="${user.name}" <%= name%>>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>生年月日</b></div> <input type="date" class="col-md-4" name="birthDate" value="${user.birthDate}">
	</div>

<%-- hiddenパラメータ [p154, 3-14] （見えないformの入力項目）--%>
	<%-- <input type="hidden" value="値" name="名前"> ※この部品は画面には表示されない --%>

		<input type="hidden" value="${user.id}" name="id">


	<div align="center">
		<input type="submit" value="更新">
	</div>

</form>

<div class="row">
	<div class="col-md-1"></div>
	<a href="UserListServlet">戻る</a>
</div>

</body>
</html>