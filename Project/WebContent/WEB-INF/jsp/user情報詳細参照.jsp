<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <%-- EL式–カスタムタグ[4-28] --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照画面</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

<header>

	<ul>
		<li>ユーザ名  ${userInfo.name} さん</li>
		<li class="dropdown"> <a href="LogoutServlet" class="btn1">ログアウト</a></li>
	</ul>

</header>

<h1 align="center">ユーザ情報詳細参照</h1>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2"><b>ログインID</b></div> <div class="col-md-4">${user.loginId}</div>	<%-- user ⇒ setAttribure("user") --%>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2"><b>ユーザ名</b></div> <div class="col-md-4">${user.name}</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>生年月日</b></div> <div class="col-md-4">${user.birthDate}</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>登録日時</b></div> <div class="col-md-4">${user.createDate}</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>更新日時</b></div> <div class="col-md-4">${user.updateDate}</div>
	</div>

<div class="row">
	<div class="col-md-1"></div>
	<a href="UserListServlet">戻る</a>
</div>

</body>
</html>