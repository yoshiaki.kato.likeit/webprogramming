<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <%-- EL式–カスタムタグ[4-28] --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

<form class="form-signin" action="LoginServlet" method="post">

	<div class="text-center mb-4">
		<h1 class="h3 mb-3 font-weight-normal">ログイン画面</h1>
	</div>

<c:if test="${errMsg != null}" >
	<div class="alert alert-danger" role="alert">
		${errMsg}
	</div>
</c:if>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label">ログインID</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" name="loginId" placeholder="IDを入力">
		</div>
	</div>

	<div class="form-group row">
		<label class="col-sm-2 col-form-label">パスワード</label>
		<div class="col-sm-4">
			<input type="password" class="form-control" name="password" placeholder="Passwordを入力">
		</div>
		<button class="btn btn-primary" type="submit">ログイン</button>
	</div>

</form>

</body>
</html>


<%--
[Chapter04]
HTML(HyperText Markup Language)：Webページに表示するデータを記述
CSS(Cascating Style Sheets)：データの色や大きさといった見栄えを記述

HTMLにCSSを適用させてWebページを作成する方法
1. タグのstyle属性に記述して、部分的に適用する
2. <style>タグの要素に記述して、文書単位に設定する
3. 拡張子「.css」のファイルに記述して、HTMLからそのファイルにリンクすることで設定する（→レイアウトが統一される）

CSSフレームワーク：よく使うスタイルをあらかじめ記述し、それを利用するといった使われ方
「SkyBlue CSS Framework」https://stanko.github.io/skyblue/
http://themes-pixeden.com/font-demos/7-stroke/index.html



--%>