<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認画面</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

<header>

	<ul>
		<li>ユーザ名  ${userInfo.name} さん</li>
		<li class="dropdown"> <a href="LogoutServlet" class="btn1">ログアウト</a></li>
	</ul>

</header>

<h1 align="center">ユーザ削除確認</h1>

<p style="padding-left:5em">
	ログインID：${user.loginId}<br>を本当に削除してよろしいですか。
</p>


<form action="UserDeleteServlet" method="post">

<%-- hiddenパラメータ [p154, 3-14] （見えないformの入力項目）--%>
	<%-- <input type="hidden" value="値" name="名前"> ※この部品は画面には表示されない --%>

		<input type="hidden" value="${user.id}" name="id">

<div align="center">
	<a href="UserListServlet" > <input type="button" value="キャンセル"> </a>

	<input type="submit" value="OK">

</div>

</form>


</body>
</html>