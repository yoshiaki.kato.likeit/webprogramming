<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> <%-- EL式–カスタムタグ[4-28] --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ一覧画面</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

<header>

	<ul>
		<li>ユーザ名  ${userInfo.name} さん</li>
		<li class="dropdown"> <a href="LogoutServlet" class="btn1">ログアウト</a></li>
	</ul>

</header>

<h1 align="center">ユーザ一覧</h1>

<div class="row">
	<div class="col-md-10"></div>
	<a href="UserCreateServlet">新規登録</a>
</div>

<form action="UserListServlet" method="post">

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-2"><b>ログインID</b></div> <input type="text" class="col-md-4" name="loginId">
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>ユーザ名</b></div> <input type="text" class="col-md-4" name="name">
	</div>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-sm-2"><b>生年月日</b></div> <input type="date" class="col-md-2" name="birthDateStart">
		<p>～</p> <input type="date" class="col-md-2" name="birthDateEnd">
	</div>

	<div class="row">
		<div class="col-md-10"></div>
		<input type="submit" value="検索">
	</div>

</form>

<br>

<hr width="90%" color="#000000">

<br>

<style>
	table {width:90%; border-collapse:collapse;}
	th {border:solid 1px; padding:20px 10px; background-color:#c0c0c0;}
	td {border:solid 1px; padding:20px 10px;}
</style>

<table align="center">

	<tr>
		<th>ログインID</th> <th>ユーザ名</th> <th>生年月日</th> <th></th>
	</tr>

<%-- <c:forEach>タグ（拡張for文）--%>
	<%-- <c:forEach var="変数名" items="インスタンスの集合" > --%>
	<%-- user型のインスタンスを格納したリストを、要素数分繰り返す [4-29 EL式-カスタムタグ] p365 --%>

<c:forEach var="user" items="${userList}" >		<%-- userList ⇒ [UserListServlet] setAttribure("userList") --%>
	<tr>
		<td>${user.loginId}</td> <td>${user.name}</td> <td>${user.birthDate}</td>

		<!-- TODO 未実装；ログインボタンの表示制御を行う -->
		<td>

	<c:if test="${userInfo.loginId == 'admin'}">
	<%-- カスタムタグは<c:if test="${userInfo.loginId.equals(admin)}">ではなく == を使う--%>

			<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a> <%--URLの後ろにパラメータを付与--%>
			<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
			<a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>

	</c:if>

	<c:if test="${userInfo.loginId != 'admin'}">
			<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
		<c:if test="${userInfo.loginId == user.loginId}">
			<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
		</c:if>
	</c:if>

		</td>

	</tr>
</c:forEach>


</table>

</body>
</html>