package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author kato
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginID
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく

			// SERECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// パスワードを暗号化するメソッドを実行
			String encryptionPassword = encryptionPassword(password);

			// SERECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, encryptionPassword);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				// ログイン失敗（一件も結果が見つからなかったらnullを返す）
			if (!rs.next()) {   // next()で参照先を次の要素に移動 [18.コレクション -5]
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
				/* ログイン成功（ResultSetからカラム名を指定してレコード内のデータを取り出し、
			 					 Beansインスタンスのフィールドにセットして返す）*/
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll(){
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
//			String sql = "SELECT * FROM user";
			String sql = "SELECT * FROM user WHERE id >= 2";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容をUserインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);   // 全件取得したデータをリスト化してListにつめる
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザ情報を検索する
	 * @return
	 */
	public List<User> findSearch(String loginIdP, String nameP, String birthDateStart, String birthDateEnd) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
//			String sql = "SELECT * FROM user";
			String sql = "SELECT * FROM user WHERE id >= 2 ";

			// 入力項目が空文字でない(＝入力されている)場合、SQL文に検索条件を追加する

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";   // 完全一致検索
			}

			if(!nameP.equals("")) {
				sql += " AND name LIKE '%" + nameP + "%'";   // 部分一致検索 [05.SQL応用 -24]
			}

			if(!birthDateStart.equals("")) {
				sql += " AND birth_date >= '" + birthDateStart + "'";   // 比較演算子 [03.SQL基礎 -23]
			}

			if(!birthDateEnd.equals("")) {
				sql += " AND birth_date <= '" + birthDateEnd + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容をUserインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);   // 取得したデータをリスト化してListにつめる
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザ情報を新規登録する
	 * @return
	 */
	public int findByCreateInfo
	(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int result = 0 ;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備（データの登録）
			String insertSQL
			= "INSERT INTO user(login_id, password, name, birth_date, create_Date, update_date) VALUES(?,?,?,?,now(),now())";
																	// NOW() 関数：現在の日付・時刻（タイムスタンプ）を取得

			// ステートメント生成（SQL文を実行するためにステートメントを発行）
			pStmt = conn.prepareStatement(insertSQL);

			// パスワードを暗号化するメソッドを実行
			String encryptionPassword = encryptionPassword(password);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, encryptionPassword);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			// 登録SQLを実行
			result = pStmt.executeUpdate();   // executeUpdateメソッドの戻り値は、レコードの"数" [DBとの連携-9]

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する
			if (result > 0) {
				System.out.println("データの登録に成功しました。");
			} else {
				System.out.println("データの登録に失敗しました。");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの登録に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;   // 追加されたレコードの数を返す
	}


	/**
	 * 1人のユーザ情報を取得する
	 * @return
	 */
	public User findByDetailInfo(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SERECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SERECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();    // ResultSetにはSQLを実行した結果が入る

			// データをUserインスタンスのフィールドに追加
			while (rs.next()) {
				int id2 = rs.getInt("id");   // UserUpdateServletでidを使用   ※getInt, 変数idを重複しない名前id2に変更
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				return new User(id2, loginId, name, birthDate, createDate, updateDate);
					// newでUserインスタンスを生成した直後に自動でメソッドが実行（コンストラクタ [11-8]）
					// 仮引数と実引数のデータ型と個数が同じUserメソッドを呼び出す（オーバーロード [7-29]）
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * ユーザ情報を更新する
	 * @return
	 */
	public int findByUpdateInfo
	(String id, String password, String name, String birthDate) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int result = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備（データの更新）
				// UPDATE テーブル名 SET カラム名1 = 値1, カラム名2 = 値2, ... WHERE 条件式;
			String updateSQL
			= "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE id = ?";

			// ステートメント生成（SQL文を実行するためにステートメントを発行）
			pStmt = conn.prepareStatement(updateSQL);

			// パスワードを暗号化するメソッドを実行
			String encryptionPassword = encryptionPassword(password);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, encryptionPassword);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);
			// SQLを実行
			result = pStmt.executeUpdate();   // executeUpdateメソッドの戻り値は、レコードの"数" [DBとの連携-9]

			// 更新に成功した場合、「データの更新に成功しました。」と表示する
			// 更新に失敗した場合、「データの更新に失敗しました。」と表示する
			if (result > 0) {
				System.out.println("データの更新に成功しました。");
			} else {
				System.out.println("データの更新に失敗しました。");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの更新に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}


	/**
	 * ユーザ情報を更新する（passwordなし）
	 * @return
	 */
	public int findByUpdateInfo
	(String id, String name, String birthDate) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		int result = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// UPDATE文を準備（データの更新）
				// UPDATE テーブル名 SET カラム名1 = 値1, カラム名2 = 値2, ... WHERE 条件式;
			String updateSQL
			= "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

			// ステートメント生成（SQL文を実行するためにステートメントを発行）
			pStmt = conn.prepareStatement(updateSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);
			// SQLを実行
			result = pStmt.executeUpdate();   // executeUpdateメソッドの戻り値は、レコードの"数" [DBとの連携-9]

			// 更新に成功した場合、「データの更新に成功しました。」と表示する
			// 更新に失敗した場合、「データの更新に失敗しました。」と表示する
			if (result > 0) {
				System.out.println("データの更新に成功しました。");
			} else {
				System.out.println("データの更新に失敗しました。");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの更新に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}


	/**
	 * ユーザ情報を削除する
	 * @return
	 */
	public void findByDeleteInfo(String id) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// DELETE文を準備（データの削除）
				// DELETE FROM テーブル名 WHERE 条件式;
			String deleteSQL
			= "DELETE FROM user WHERE id = ?";

			// ステートメント生成（SQL文を実行するためにステートメントを発行）
			pStmt = conn.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, id);
			// SQLを実行
			int result = pStmt.executeUpdate();

			// 削除に成功した場合、「データの削除に成功しました。」と表示する
			// 削除に失敗した場合、「データの削除に失敗しました。」と表示する
			if (result > 0) {
				System.out.println("データの削除に成功しました。");
			} else {
				System.out.println("データの削除に失敗しました。");
			}

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの削除に失敗しました。");
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * パスワードを暗号化するメソッド
	 * @return
	 */
	public static String encryptionPassword(String password) {   // 静的メソッド [11-16] Staticがつくメソッドはクラスが実行する

		/** 暗号化 [DBとの連携-19]**/
		//ハッシュを生成したい元の文字列
		String source = password; // String source = 暗号化対象;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
			/* try-catchで囲んでgetInstanceの例外「NoSuchAlgorithmException」が起きた時に処理する
			 * （getInstanceを右クリック -> 宣言を開く -> throws 例外クラス名 [17-11]）
			 */
		String encryptionResult = null;

		try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			encryptionResult = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) { // 例外が起こった際の情報をNoSuchAlgorithmException型の変数eに代入
			e.printStackTrace(); // スタックトレースの内容を画面に出力する
		}
		//標準出力
		System.out.println(encryptionResult);

		return encryptionResult;   // 戻り値の利用 [07.メソッド -19]
	}


}
