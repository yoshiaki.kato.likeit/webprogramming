package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user新規登録.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定（文字化け防止，doPostの先頭に記述）
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得（jspのformで name=" " に指定したリクエストパラメータの名前を引数にする [3-17]）
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		/** パスワードとパスワード(確認)が異なる場合 **/
		if (!password.equals(confirmPassword)) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user新規登録.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 入力項目に１つでも未入力がある場合 **/
		if (loginId.equals("") || password.equals("") || name.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user新規登録.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		int result = userDao.findByCreateInfo(loginId, password, name, birthDate);

		/** 登録に失敗した場合 **/
		if (result == 0) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user新規登録.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 登録に成功した場合 **/
		// ユーザ一覧のサーブレットにリダイレクト（ユーザ一覧画面に遷移する）
		response.sendRedirect("UserListServlet");

	}

}
