package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user = userDao.findByDetailInfo(id);
		System.out.println(user);

		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user情報更新.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定（文字化け防止，doPostの先頭に記述）
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得（jspのformで name=" " に指定したリクエストパラメータの名前を引数にする [3-17]）
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		/** パスワードとパスワード(確認)が異なる場合 **/
		if (!password.equals(confirmPassword)) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");



			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user情報更新.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** ユーザー名が未入力の場合 **/
		if (name.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

//			response.sendRedirect("UserUpdateServlet?id=${user.id}");

			// リクエストスコープにパスワード以外のユーザ情報をセット

////			request.setAttribute("user.loginId", loginId);
//			request.setAttribute("user.name", name);
//			request.setAttribute("user.birthDate", birthDate);
//			request.setAttribute("user.id", id);

			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user新規登録.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();


			int result;   // 変数宣言（※if文中に宣言すると変数がif文の中でしか使えない）

			/** パスワードが空欄の場合 **/
			if(password.equals("")) {   // ※jspの入力項目に入力がされていないときnullではなく空文字（""）が入る
										// ※String型の文字列比較はequalsメソッドを使用 [16-6]
				// パスワード以外の情報を更新する
				result = userDao.findByUpdateInfo(id, name, birthDate);

			/** パスワードが入力されている場合（if文の条件を満たしていないとき） **/
			} else {
				// パスワードを含めた情報を更新する
				result = userDao.findByUpdateInfo(id, password, name, birthDate);
			}

		/** 更新に失敗した場合 **/
		if (result == 0) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// jspにフォワード（入力画面に戻る）
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user情報更新.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 更新に成功した場合 **/
		// ユーザ一覧のサーブレットにリダイレクト（ユーザ一覧画面に遷移する）
		response.sendRedirect("UserListServlet");

	}

}
