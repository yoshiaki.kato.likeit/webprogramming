package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author kato
 *
 */
public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	// ログインセッションを保持するためのコンストラクタ
	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
//		super();
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}


//	public User(String loginId, String password, String name, Date birthDate,  String createDate, String updateDate) {
//		this.loginId = loginId;
//		this.name = name;
//		this.birthDate = birthDate;
//		this.password = password;
//		this.createDate = createDate;
//		this.updateDate = updateDate;
//	}




	// 1人のユーザ情報データをセットするコンストラクタ
	public User(int id2, String loginId, String name, Date birthDate, String createDate, String updateDate) {
		this.id = id2;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}

/* [section3_3 p.61~]
 * JavaBeansとは「再利用可能なクラスを定義するために規定された技術仕様」
 * 以下に挙げた要件に従って、クラスを定義する。
 * 　・public修飾子の付いた、引数なしのコンストラクタを定義する
 *
 * 　・private修飾子の付いたメンバ変数に対応した、publicなアクセサを命名規則（変数名の先頭を大文字にする）に従って定義する
 * 　　（アクセサとは、private修飾子がついたメンバ変数を参照または更新できるメソッドのこと。
 * 　　　メンバ変数の値を返すgetterメソッドと、引数に受け取った値でメンバ変数を更新するsetterメソッドで構成される。）
 *
 * 　・シリアライズを可能にする（クラス定義の後ろに「implements Serializable」を付ける）
 * 　　⇒シリアライズ可能なクラスは、プログラム実行中のオブジェクトが持つデータを保存・読込みできる
 */